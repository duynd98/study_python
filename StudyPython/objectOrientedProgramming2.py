class Animal:

    def __init__(self):
        print("Animal Created")

    @staticmethod
    def whoAmI():
        print("Animal")

    @staticmethod
    def eat():
        print("Eating")


class Dog(Animal):

    def __init__(self):
        # Animal.__init__(self)
        print("Dog Created")
    
    @staticmethod
    def bark():
        print("Gau Gau")

    @staticmethod
    def eat():
        print("Dog Eating")


# animal = Animal()
# animal.whoAmI()
# animal.eat()
myDog = Dog()
myDog.whoAmI()
myDog.eat()
myDog.bark()

