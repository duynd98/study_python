class Dog:
    species = "Mammal"

    def __init__(self, breed, name):
        self.breed = breed
        self.name = name


myDog = Dog(breed="Person", name="Le Nguyen")

print(myDog.breed)
print(myDog.name)
print(myDog.species)
