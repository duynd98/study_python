# x = 25
#
#
# def my_func():
#     x = 50
#     return x
#
#
# print(my_func())

# name = "Do you love me ?"
#
#
# def greet():
#     name = "Le Nguyen"
#
#     def hello():
#         print("Hello " + name)
#
#     hello()
#
#
# greet()
# print(name)
#
# x = 50
#
#
# def func(x):
#     print("x is", x)
#     x = 1000
#     print("local x changed to", x)
#
#
# func(x)
# print(x)

x = 50


def func():
    global x
    x = 1000
    return x


print("before function call, x is:", x)
x = func()
print("after function call, x is:", x)

# a = 1
#
#
# def add():
#     global a
#     a = a + 9
#     print(a)



