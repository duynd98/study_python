myPets = ['Duy', 'Trang', 'Hoa']
print("Enter a pet name:")
while True:
    name = input()
    if name not in myPets:
        print("I do not have a pet named " + name)
    else:
        print(name + " is my pet.")
        break
